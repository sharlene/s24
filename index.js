// get cube use exponent operator
let getCube = (num)=> num**3;
let cube = getCube(2);


// template literal, print get cube with message the cube of <num> is

console.log(`The cube of 2 is ${cube}`);

//  create a variable address with a value of an array containing details of an address
let address = [258, "Washington Ave NW", "California", 90011];
let [streetNum, streetName, stateName, zipCode ] = address;

// destructure array, print out message using template literal
console.log(`I live at ${streetNum} ${streetName}, ${stateName} ${zipCode}`);

// create animal object with animal detail properties
let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};


// destructure animal object and print out a message with details using template literals
let {name,type,weight,measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);

// create an array of number
let numbers = [1,2,3,4,5];

// loop through the array using for each, an arrow function to print out the numbers
numbers.forEach((number)=> console.log(number));

// create variable reduce number, using the reduce() array method and arrow function console log the sum of all the numbers
let reduceNumber = numbers.reduce((x,y)=>x+y);
console.log(reduceNumber);

// create a class of a dog and a constructor that will accept a name, age, breed
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

// instantiate new object for class dog and console log the object
let newDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(newDog);


